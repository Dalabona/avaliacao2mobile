package com.dot4t.avaliacao2mobile;

public class Parcela {
    private int nrParcela;
    private double juros;
    private double amortizacao;
    private double saldo;

    public Parcela(int nrParcela, double juros, double amortizacao, double saldo) {
        this.nrParcela = nrParcela;
        this.juros = juros;
        this.amortizacao = amortizacao;
        this.saldo = saldo;
    }

    public int getNrParcela() {
        return nrParcela;
    }

    public double getValor() {
        return juros + amortizacao;
    }

    public double getJuros() {
        return juros;
    }

    public double getAmortizacao() {
        return amortizacao;
    }

    public double getSaldo() {
        return saldo;
    }
}
