package com.dot4t.avaliacao2mobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

public class Apresentacao extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresentacao);

        Bundle bundle = getIntent().getExtras();

        TextView tvTotal = findViewById(R.id.tvTotal);
        ListView lv = findViewById(R.id.lvSimulacao);

        String tipo = bundle.getString("tipo");
        int meses = bundle.getInt("meses");
        Double valor = bundle.getDouble("valor"), juros = bundle.getDouble("juros");
        Financiamento f = new Financiamento(meses, valor, juros, tipo);

        ParcelaAdapter adater = null;
        switch (tipo){
            case "sac":
                adater = new ParcelaAdapter(getApplicationContext(), R.layout.lista, f.getParcelasSac());
                tvTotal.setText("R$" + f.getTotalSac());
                break;
            case "prime":
                adater = new ParcelaAdapter(getApplicationContext(), R.layout.lista, f.getParcelasPrime());
                tvTotal.setText("R$" + f.getTotalPrime());
                break;
        }

        lv.setAdapter(adater);

    }
}
