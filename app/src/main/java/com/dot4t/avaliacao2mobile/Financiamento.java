package com.dot4t.avaliacao2mobile;

import java.util.ArrayList;

public class Financiamento {

    private ArrayList<Parcela> parcelasPrime;
    private ArrayList<Parcela> parcelasSac;
    private int meses;
    private double valor;
    private double juros;

    public Financiamento(int meses, double valor, double juros, String tipo) {
        this.meses = meses;
        this.valor = valor;
        this.juros = juros/100;
        parcelasPrime = new ArrayList<>();
        parcelasSac = new ArrayList<>();
        calculaParcelasPRICE();
        calculaParcelasSAC();
    }

    public void calculaParcelasPRICE() {
        parcelasPrime.add(new Parcela(0, 0, 0, valor));
        double jurosAtual, amortizacao, valorRestante = valor, parcela;
        parcela = valorRestante * (juros / (1 - (1 / Math.pow((1 + juros),meses))));
        for(int i = 1; i <= meses; i++) {
            jurosAtual = valorRestante * juros;
            amortizacao = parcela - jurosAtual;
            valorRestante = valorRestante - amortizacao;
            parcelasPrime.add(new Parcela(i, jurosAtual, amortizacao, valorRestante));
        }
    }

    public void calculaParcelasSAC() {
        parcelasSac.add(new Parcela(0, 0, 0, valor));
        double jurosAtual, amortizacao, valorRestante = valor, parcela;
        for(int i = 1; i <= meses; i++) {
            jurosAtual = valorRestante * juros;
            amortizacao = (valor / meses);
            valorRestante = valorRestante - amortizacao;
            parcelasSac.add(new Parcela(i, jurosAtual, amortizacao, valorRestante));
        }
    }

    public ArrayList<Parcela> getParcelasSac() {
        return parcelasSac;
    }

    public ArrayList<Parcela> getParcelasPrime() {
        return parcelasPrime;
    }

    public String getTotalSac() {
        Double total = 0.0;
        for(Parcela p : parcelasSac) {
            total += p.getValor();
        }
        return String.format("%.2f", total);
    }

    public String getTotalPrime() {
        Double total = 0.0;
        for(Parcela p : parcelasPrime) {
            total += p.getValor();
        }
        return String.format("%.2f", total);
    }

}
