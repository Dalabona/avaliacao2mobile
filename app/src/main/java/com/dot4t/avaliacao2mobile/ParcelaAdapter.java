package com.dot4t.avaliacao2mobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class ParcelaAdapter extends ArrayAdapter<Parcela> {

    private Context contextoAtual;
    private int resourceAtual;

    public ParcelaAdapter(Context context, int resource, List<Parcela> lista) {
        super(context, resource, lista);
        contextoAtual = context;
        resourceAtual = resource;
    }

    public View getView(int position, View cv, ViewGroup parent) {

        LayoutInflater inf = LayoutInflater.from(contextoAtual);
        cv = inf.inflate(resourceAtual, parent, false);

        TextView tvMes = cv.findViewById(R.id.tvMes);
        TextView tvPrestacao = cv.findViewById(R.id.tvPrestacao);
        TextView tvAmortizacao = cv.findViewById(R.id.tvAmortizacao);
        TextView tvJuros = cv.findViewById(R.id.tvJuros);
        TextView tvSaldo = cv.findViewById(R.id.tvSaldo);

        tvMes.setText(Integer.toString(getItem(position).getNrParcela()));
        tvPrestacao.setText(String.format("%.2f",getItem(position).getValor()));
        tvAmortizacao.setText(String.format("%.2f",getItem(position).getAmortizacao()));
        tvJuros.setText(String.format("%.2f",getItem(position).getJuros()));
        tvSaldo.setText(String.format("%.2f",getItem(position).getSaldo()));

        return cv;
    }
}
