package com.dot4t.avaliacao2mobile;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    private EditText etValor;
    private EditText etMeses;
    private TextView etJuros;
    private SeekBar sbJuros;
    private Button btCalculaSac, btCalculaPrime;

    private static NumberFormat pf = NumberFormat.getPercentInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etValor = findViewById(R.id.etValor);
        etMeses = findViewById(R.id.etMeses);
        etJuros = findViewById(R.id.etJuros);
        sbJuros = findViewById(R.id.sbJuros);
        btCalculaSac = findViewById(R.id.btCalculaSac);
        btCalculaPrime = findViewById(R.id.btCalculaPrime);

        etValor.setText("1000.00");
        etMeses.setText("1");
        etJuros.setText("0.00%");

        pf.setMinimumFractionDigits(2);

        sbJuros.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                etJuros.setText(pf.format(progress * 0.001));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        btCalculaSac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jurosTxt = etJuros.getText().toString();
                jurosTxt = jurosTxt.replace("%", "");
                Intent it = new Intent(getApplicationContext(), Apresentacao.class);
                Bundle b = new Bundle();
                b.putString("tipo", "sac");
                b.putInt("meses", Integer.parseInt(etMeses.getText().toString()));
                b.putDouble("valor", Double.parseDouble(etValor.getText().toString()));
                b.putDouble("juros", Double.parseDouble(jurosTxt));
                it.putExtras(b);
                startActivity(it);
            }
        });

        btCalculaPrime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String jurosTxt = etJuros.getText().toString();
                jurosTxt = jurosTxt.replace("%", "");
                Intent it = new Intent(getApplicationContext(), Apresentacao.class);
                Bundle b = new Bundle();
                b.putString("tipo", "prime");
                b.putInt("meses", Integer.parseInt(etMeses.getText().toString()));
                b.putDouble("valor", Double.parseDouble(etValor.getText().toString()));
                b.putDouble("juros", Double.parseDouble(jurosTxt));
                it.putExtras(b);
                startActivity(it);
            }
        });

    }
}
